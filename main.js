class person {
    constructor(name, color, walk) {
        this.name = name;
        this.color = color;
        this.walk = walk;
    }
}

const person1 = new person ("virgule", "bleu", "et toi ça va?" );
console.log(person1);

const person2 = new person ("bouboule", "vert", "ça te regardes!!!!");
console.log(person2);

const section = document.querySelector("#section");

const img01 = document.createElement("img");
img01.className = "imgBou";
img01.src ="./bouboule.jpg";
section.appendChild(img01);

const spanBouboule = document.createElement ("div");
let texte = document.createTextNode(person2.walk);
spanBouboule.appendChild(texte);
section.appendChild(spanBouboule);

const spanVirgule = document.createElement ("div");
let text = document.createTextNode(person1.walk);
spanVirgule.appendChild(text);
section.appendChild(spanVirgule);

const img02 = document.createElement("img");
img02.src ="./virgule.jpg";
section.appendChild(img02);









